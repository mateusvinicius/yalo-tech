class MyComponent extends React.PureComponent {
    constructor(props) {
      super(props);
    }
  
    render() {
      return (
        <div>
            Hello {props.name}
        </div>
      )
    }
}


const MyComponent = (props) => {
    return (
        <div>
            Hello {props.name}
        </div>
    )
}

export default React.memo(MyComponent)