
const withColorChange = (WrappedComponent) => {
  return class WithColorChange extends Component {
    constructor(props) {
      super(props);

      this.state = {
        color: 'blue',
      };
    }

    changeColor = () => {
      const newColor = this.state.color === 'blue' ? 'green' : 'blue';
      this.setState({ color: newColor });
    };

    render() {
      return (
        <div style={{ color: this.state.color }}>
          <WrappedComponent
            {...this.props}
            color={this.state.color}
            changeColor={this.changeColor}
          />
        </div>
      );
    }
  };
};

// Create a component to be wrapped by the HOC
class MyComponent extends React.Component {
  render() {
    return (
      <div>
        <h1>Wrapped Component</h1>
        <p>Color: {this.props.color}</p>
        <button onClick={this.props.changeColor}>Change Color</button>
      </div>
    );
  }
}

// Apply the HOC to the component
const MyComponentWithColorChange = withColorChange(MyComponent);

// Usage
class App extends React.Component {
  render() {
    return (
      <div>
        <h1>HOC Example</h1>
        <MyComponentWithColorChange />
      </div>
    );
  }
}




class ConsultCustomerPlansScreen extends React.Component {
  constructor() {
    super();
  }

  render() {
    
    return (
      <Grid container justify="center">
        Hello World
      </Grid>
    );
  }
}


const mapDispatchToProps = dispatch =>
  bindActionCreators({
    ...consultCustomerPlansActions,
    ...updateProfileActions,
    ...saleActions,
  }, dispatch)

const mapStateToProps = ({ consultCustomerPlans, updateProfile, sales }) => ({
  ...consultCustomerPlans,
  ...updateProfile,
  ...sales
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConsultCustomerPlansScreen)