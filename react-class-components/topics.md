# React Class Components

## O que é um class component
- Diferença entre function component
- Sintaxe básica

## OOP
- Herança, encapsulamento, classe
- Composição vs Herança (https://legacy.reactjs.org/docs/composition-vs-inheritance.html)

## Lifecycle
- Lifecycle básico:
![baisc-lifecycle](image.png)

- Lifecycle completo:
![complete-lifecycle](image-1.png)

- Etapas:
    - Render: Fase de reconciliation. React determina quais mudanças precisam ser feitas no virtual DOM baseado nos updates dos estados dos componentes ou das props. Computa o novo virtual dom.
    - Pre-commit: Checa e valida as atualizações antes de modificar a UI, por ex: checando se algum componente tem erro ou se alguma atualização do schedule de updates foi invalidada. Mantém a consistencia do estado e props.
    - Commit: Aplica as mudanças computadas no DOM real e dispara os lifecycles side effects.

## Métodos do lifecycle
- Mounting
    - constructor
    - getDerivedStateFromProps
    - render
    - componentDidMount: como um useEffect com array de dependencias vazio

- Updating
    - getDerivedStateFromProps
    - shouldComponentUpdate: Define quando um component deve ser atualizado, otimização de performance, não deve ser usado para previnir rerenderização, o PureComponent deve ser usado pra isso
    - render
    - getSnapshotBeforeUpdate
    - componentDidUpdate

- Unmounting
    - componentWillUnmount

## Outros tipos de components
- PureComponent: equivalente a memo()
- HOC: High Order Component