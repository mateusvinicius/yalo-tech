class MyComponent extends React.Component {
    constructor(props) {
      super(props);
    }

    componentDidMount() {
        console.log('2º a ser chamado')
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('3º a ser chamado')
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('4º a ser chamado')
    }

    componentWillUnmount() {
        console.log('Último a ser chamado')
    }
  
    render() {
        console.log('1ª a ser chamado')

      return (
        <div>
            Hello {props.name}
        </div>
      )
    }
}