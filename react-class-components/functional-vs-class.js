class MyComponent extends React.Component {
    constructor(props) {
      super(props);
    }
  
    render() {
      return (
        <div>
            Hello {props.name}
        </div>
      )
    }
}

const MyComponent = (props) => {
    return (
        <div>
            Hello {props.name}
        </div>
    )
}