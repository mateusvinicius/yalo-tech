class BaseComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
    };
  }

  incrementCount = () => {
    this.setState((prevState) => ({
      count: prevState.count + 1,
    }));
  };

  render() {
    return (
      <div>
        <h2>Base Component</h2>
        <p>Count: {this.state.count}</p>
        <button onClick={this.incrementCount}>Increment</button>
      </div>
    );
  }
}


class SpecializedComponent extends BaseComponent {
  render() {
    return (
      <div>
        <h2>Specialized Component</h2>
        <p>Count: {this.state.count}</p>
        <button onClick={this.incrementCount}>Increment</button>
      </div>
    );
  }
}


class App extends Component {
  render() {
    return (
      <div>
        <h1>Inheritance Example</h1>
        <BaseComponent />
        <SpecializedComponent/>
      </div>
    );
  }
}

export default App;
