class MyComponent extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        name: 'Foo',
        class: 'Bar'
      }
    }

    updateName() {
        this.setState((state, props) => {
            return {
                ...state,
                name: 'Baz'
            }
        })

        // batch update
    }
  
    render() {
      return (
        <div>
            Hello {state.name}

            <button onClick={() => this.updateName()}>Change name</button>
        </div>
      )
    }
}