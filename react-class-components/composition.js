
class Counter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
    };
  }

  incrementCount = () => {
    this.setState((prevState) => ({
      count: prevState.count + 1,
    }));
  };

  render() {
    return (
      <div>
        <p>Count: {this.state.count}</p>
        <button onClick={this.incrementCount}>Increment</button>
      </div>
    );
  }
}


class SpecializedComponent extends Component {
  render() {
    return (
      <div>
        <h2>Specialized Component</h2>
        <Counter />
      </div>
    );
  }
}


class App extends Component {
  render() {
    return (
      <div>
        <h1>Composition Example</h1>
        <div>
        <h2>Base Component</h2>
            <Counter />
        </div>
        <SpecializedComponent />
      </div>
    );
  }
}
