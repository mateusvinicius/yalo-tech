class MyComponent extends React.Component {
    constructor(props) {
      super(props);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if(nextProps.name === 'Mateus') {
            return false
        }

        return true
    }
  
    render() {
      return (
        <div>
            Hello {props.name}

            <button onClick={() => this.forceUpdate()}>Force update</button>
        </div>
      )
    }
}